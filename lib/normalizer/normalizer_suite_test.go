package normalizer

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestNormalizer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Normalizer Suite")
}

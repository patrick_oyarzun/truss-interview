package normalizer_test

import (
	"encoding/csv"
	"truss-interview/lib/normalizer"

	_ "embed"

	"unicode"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gbytes"
)

// Generated from sample.csv
var sampleData = [][]string{
	{"Timestamp", "Address", "ZIP", "FullName", "FooDuration", "BarDuration", "TotalDuration", "Notes"},
	{"4/1/11 11:00:00 AM", "123 4th St, Anywhere, AA", "94121", "Monkey Alberto", "1:23:32.123", "1:32:33.123", "zzsasdfa", "I am the very model of a modern major general"},
	{"3/12/14 12:00:00 AM", "Somewhere Else, In Another Time, BB", "1", "Superman übertan", "111:23:32.123", "1:32:33.123", "zzsasdfa", "This is some Unicode right here. ü ¡! 😀"},
	{"2/29/16 12:11:11 PM", "111 Ste. #123123123", "1101", "Résumé Ron", "31:23:32.123", "1:32:33.123", "zzsasdfa", "🏳️🏴🏳️🏴"},
	{"1/1/11 12:00:01 AM", "This Is Not An Address, BusyTown, BT", "94121", "Mary 1", "1:23:32.123", "0:00:00.000", "zzsasdfa", "I like Emoji! 🍏🍎😍"},
	{"12/31/16 11:59:59 PM", "123 Gangnam Style Lives Here, Gangnam Town", "31403", "Anticipation of Unicode Failure", "1:23:32.123", "1:32:33.123", "zzsasdfa", "I like Math Symbols! ≱≰⨌⊚"},
	{"11/11/11 11:11:11 AM", "überTown", "10001", "Prompt Negotiator", "1:23:32.123", "1:32:33.123", "zzsasdfa", "I’m just gonna say, this is AMAZING. WHAT NEGOTIATIONS."},
	{"5/12/10 4:48:12 PM", "Høøük¡", "1231", "Sleeper Service", "1:23:32.123", "1:32:33.123", "zzsasdfa", "2/1/22"},
	{"10/5/12 10:31:11 PM", "Test Pattern Town, Test Pattern, TP", "121", "株式会社スタジオジブリ", "1:23:32.123", "1:32:33.123", "zzsasdfa", "1:11:11.123"},
	{"3/12/16 11:01:00 PM", "The Moon", "11", "HERE WE GO", "1:23:32.123", "1:32:33.123", "zzsasdfa", ""},
}

var header = sampleData[0]

var _ = Describe("Normalizer", func() {
	var subject *normalizer.Normalizer
	var in, out *gbytes.Buffer

	BeforeEach(func() {
		var err error
		subject, err = normalizer.NewNormalizer()
		Expect(err).NotTo(HaveOccurred())
		in = gbytes.NewBuffer()
		out = gbytes.NewBuffer()
	})

	writeRows := func(rows [][]string) {
		writer := csv.NewWriter(in)
		for _, row := range rows {
			Expect(writer.Write(row)).To(Succeed())
		}
		writer.Flush()
	}

	readRows := func() [][]string {
		reader := csv.NewReader(out)
		rows, err := reader.ReadAll()
		Expect(err).NotTo(HaveOccurred())
		// Drop header
		return rows[1:]
	}

	Describe("Header behavior", func() {
		It("Passes the header along to stdout", func() {
			stderr := gbytes.NewBuffer()
			writeRows(sampleData)
			subject.Normalize(in, out, stderr)
			for _, name := range header {
				Expect(out).To(gbytes.Say(name))
			}
			Expect(stderr.Contents()).To(BeEmpty())
		})
	})

	// Safe Assumptions

	// * The input document is in UTF-8.
	// * Invalid characters can be replaced with the Unicode Replacement Character. If that replacement makes data invalid (for example, because it turns a date field into something unparseable), print a warning to `stderr` and drop the row from your output.
	// * Times that are missing timezone information are in `US/Pacific`.
	// * The sample data we provide contains all date and time format variants you will need to handle.
	// * Any type of line endings are permissible in the output.
	Describe("Normalize", func() {

		// * The `Timestamp` column should be formatted in RFC3339 format.
		// * The `Timestamp` column should be assumed to be in US/Pacific time;
		//   please convert it to US/Eastern.
		It("Formats Timestamps in RFC3339 Format, Eastern Timezone", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][0]).To(Equal("2011-04-01T14:00:00-04:00"))
			Expect(result[1][0]).To(Equal("2014-03-12T03:00:00-04:00"))
			Expect(result[2][0]).To(Equal("2016-02-29T15:11:11-05:00"))
			Expect(result[3][0]).To(Equal("2011-01-01T03:00:01-05:00"))
			Expect(result[4][0]).To(Equal("2017-01-01T02:59:59-05:00"))
			Expect(result[5][0]).To(Equal("2011-11-11T14:11:11-05:00"))
			Expect(result[6][0]).To(Equal("2010-05-12T19:48:12-04:00"))
			Expect(result[7][0]).To(Equal("2012-10-06T01:31:11-04:00"))
			Expect(result[8][0]).To(Equal("2016-03-13T03:01:00-04:00"))
		})

		// * All `ZIP` codes should be formatted as 5 digits. If there are less
		//   than 5 digits, assume 0 as the prefix.
		It("Formats zip as 0-padded, 5-digit numbers", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][2]).To(Equal("94121"))
			Expect(result[1][2]).To(Equal("00001"))
			Expect(result[2][2]).To(Equal("01101"))
			Expect(result[3][2]).To(Equal("94121"))
			Expect(result[4][2]).To(Equal("31403"))
			Expect(result[5][2]).To(Equal("10001"))
			Expect(result[6][2]).To(Equal("01231"))
			Expect(result[7][2]).To(Equal("00121"))
			Expect(result[8][2]).To(Equal("00011"))
		})

		// * The `FullName` column should be converted to uppercase. There will be
		//   non-English names.
		It("FullName is converted to uppercase", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][3]).To(Equal("MONKEY ALBERTO"))
			Expect(result[1][3]).To(Equal("SUPERMAN ÜBERTAN"))
			Expect(result[2][3]).To(Equal("RÉSUMÉ RON"))
			Expect(result[3][3]).To(Equal("MARY 1"))
			Expect(result[4][3]).To(Equal("ANTICIPATION OF UNICODE FAILURE"))
			Expect(result[5][3]).To(Equal("PROMPT NEGOTIATOR"))
			Expect(result[6][3]).To(Equal("SLEEPER SERVICE"))
			Expect(result[7][3]).To(Equal("株式会社スタジオジブリ"))
			Expect(result[8][3]).To(Equal("HERE WE GO"))
		})

		// * The `Address` column should be passed through as is, except for
		//   Unicode validation. Please note there are commas in the Address
		//   field; your CSV parsing will need to take that into account. Commas
		//   will only be present inside a quoted string.
		It("Address is passed through unchanged", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][1]).To(Equal("123 4th St, Anywhere, AA"))
			Expect(result[1][1]).To(Equal("Somewhere Else, In Another Time, BB"))
			Expect(result[2][1]).To(Equal("111 Ste. #123123123"))
			Expect(result[3][1]).To(Equal("This Is Not An Address, BusyTown, BT"))
			Expect(result[4][1]).To(Equal("123 Gangnam Style Lives Here, Gangnam Town"))
			Expect(result[5][1]).To(Equal("überTown"))
			Expect(result[6][1]).To(Equal("Høøük¡"))
			Expect(result[7][1]).To(Equal("Test Pattern Town, Test Pattern, TP"))
			Expect(result[8][1]).To(Equal("The Moon"))
		})

		// * The `FooDuration` and `BarDuration` columns are in HH:MM:SS.MS
		//   format (where MS is milliseconds); please convert them to the
		//   total number of seconds.
		It("Converts FooDuration to total seconds", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][4]).To(Equal("5012.123"))
			Expect(result[1][4]).To(Equal("401012.123"))
			Expect(result[2][4]).To(Equal("113012.123"))
			Expect(result[3][4]).To(Equal("5012.123"))
			Expect(result[4][4]).To(Equal("5012.123"))
			Expect(result[5][4]).To(Equal("5012.123"))
			Expect(result[6][4]).To(Equal("5012.123"))
			Expect(result[7][4]).To(Equal("5012.123"))
			Expect(result[8][4]).To(Equal("5012.123"))
		})

		It("Converts BarDuration to total seconds", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][5]).To(Equal("5553.123"))
			Expect(result[1][5]).To(Equal("5553.123"))
			Expect(result[2][5]).To(Equal("5553.123"))
			Expect(result[3][5]).To(Equal("0"))
			Expect(result[4][5]).To(Equal("5553.123"))
			Expect(result[5][5]).To(Equal("5553.123"))
			Expect(result[6][5]).To(Equal("5553.123"))
			Expect(result[7][5]).To(Equal("5553.123"))
			Expect(result[8][5]).To(Equal("5553.123"))
		})

		// * The `TotalDuration` column is filled with garbage data. For each
		//   row, please replace the value of `TotalDuration` with the sum of
		//   `FooDuration` and `BarDuration`.
		It("Replaces TotalDuration with FooDuration + BarDuration", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][6]).To(Equal("10565.246"))
			Expect(result[1][6]).To(Equal("406565.246"))
			Expect(result[2][6]).To(Equal("118565.246"))
			Expect(result[3][6]).To(Equal("5012.123"))
			Expect(result[4][6]).To(Equal("10565.246"))
			Expect(result[5][6]).To(Equal("10565.246"))
			Expect(result[6][6]).To(Equal("10565.246"))
			Expect(result[7][6]).To(Equal("10565.246"))
			Expect(result[8][6]).To(Equal("10565.246"))
		})

		// * The `Notes` column is free form text input by end-users; please do
		//   not perform any transformations on this column. If there are invalid
		//   UTF-8 characters, please replace them with the Unicode Replacement
		//   Character.
		It("Replaces TotalDuration with FooDuration + BarDuration", func() {
			writeRows(sampleData)
			subject.Normalize(in, out, GinkgoWriter)
			result := readRows()

			Expect(result[0][7]).To(Equal("I am the very model of a modern major general"))
			Expect(result[1][7]).To(Equal("This is some Unicode right here. ü ¡! 😀"))
			Expect(result[2][7]).To(Equal("🏳️🏴🏳️🏴"))
			Expect(result[3][7]).To(Equal("I like Emoji! 🍏🍎😍"))
			Expect(result[4][7]).To(Equal("I like Math Symbols! ≱≰⨌⊚"))
			Expect(result[5][7]).To(Equal("I’m just gonna say, this is AMAZING. WHAT NEGOTIATIONS."))
			Expect(result[6][7]).To(Equal("2/1/22"))
			Expect(result[7][7]).To(Equal("1:11:11.123"))
			Expect(result[8][7]).To(Equal(""))
		})
	})

	Describe("Invalid data", func() {

		var stderr *gbytes.Buffer

		BeforeEach(func() {
			stderr = gbytes.NewBuffer()
		})

		// * The entire CSV is in the UTF-8 character set.
		It("Replaces characters with the unicode replacement character", func() {
			// Invalid continuation byte
			// From https://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt
			invalid := []byte{0x80}

			writeRows([][]string{
				header,
				{"1/1/01 01:00:00 AM", string(invalid), "70506", string(invalid), "1:00:00.00", "1:00:00.00", string(invalid), string(invalid)},
			})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result[0][0]).To(Equal("2001-01-01T04:00:00-05:00"))
			Expect(result[0][1]).To(Equal(string(unicode.ReplacementChar)))
			Expect(result[0][2]).To(Equal("70506"))
			Expect(result[0][3]).To(Equal(string(unicode.ReplacementChar)))
			Expect(result[0][4]).To(Equal("3600"))
			Expect(result[0][5]).To(Equal("3600"))
			Expect(result[0][6]).To(Equal("7200"))
			Expect(result[0][7]).To(Equal(string(unicode.ReplacementChar)))
		})

		It("Drops rows with bad timestamps", func() {
			var row = make([]string, len(sampleData[1]))
			copy(row, sampleData[1])
			row[0] = "Not a timestamp"

			writeRows([][]string{header, row})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid Timestamp"))
			Expect(stderr).To(gbytes.Say("Not a timestamp"))
		})

		It("Drops rows with bad zip codes", func() {
			var row = make([]string, len(sampleData[1]))
			copy(row, sampleData[1])
			row[2] = "Not a zip code"

			writeRows([][]string{header, row})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid ZipCode"))
			Expect(stderr).To(gbytes.Say("Not a zip code"))
		})

		It("Drops rows with bad foo durations", func() {
			var row = make([]string, len(sampleData[1]))
			copy(row, sampleData[1])
			row[4] = "Not a duration"

			writeRows([][]string{header, row})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid FooDuration"))
			Expect(stderr).To(gbytes.Say("Not a duration"))
		})

		It("Drops rows with bad bar durations", func() {
			var row = make([]string, len(sampleData[1]))
			copy(row, sampleData[1])
			row[5] = "Not a duration"

			writeRows([][]string{header, row})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid BarDuration"))
			Expect(stderr).To(gbytes.Say("Not a duration"))
		})

		It("Drops rows with two few columns", func() {
			writeRows([][]string{
				header,
				make([]string, len(header)-1),
			})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid number of columns"))
		})

		It("Drops rows with two many columns", func() {
			writeRows([][]string{
				header,
				make([]string, len(header)+1),
			})
			subject.Normalize(in, out, stderr)
			result := readRows()

			Expect(result).To(BeEmpty())
			Expect(stderr).To(gbytes.Say("line 2"))
			Expect(stderr).To(gbytes.Say("invalid number of columns"))
		})
	})
})

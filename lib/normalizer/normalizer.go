package normalizer

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
	"unicode"
)

func NewNormalizer() (*Normalizer, error) {
	pacific, err := time.LoadLocation("US/Pacific")
	if err != nil {
		return nil, err
	}

	eastern, err := time.LoadLocation("US/Eastern")
	if err != nil {
		return nil, err
	}

	return &Normalizer{
		inputTimezone:  pacific,
		outputTimezone: eastern,
	}, nil
}

type Normalizer struct {
	inputTimezone  *time.Location
	outputTimezone *time.Location
}

func (n *Normalizer) Normalize(stdin io.Reader, stdout, stderr io.Writer) error {

	lineNumber := 1
	reader := csv.NewReader(stdin)
	reader.FieldsPerRecord = 8

	writer := csv.NewWriter(stdout)
	defer writer.Flush()

	header, err := reader.Read()
	if err != nil {
		return err
	}
	writer.Write(header)

	for {
		lineNumber++
		record, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			// Distinguish CSV format errors from io errors
			if errors.Is(err, csv.ErrBareQuote) ||
				errors.Is(err, csv.ErrFieldCount) ||
				errors.Is(err, csv.ErrQuote) ||
				errors.Is(err, csv.ErrTrailingComma) {
				fmt.Fprintf(stderr, "Invalid record at line %d: %s\n", lineNumber, err)
				continue
			}
			fmt.Fprintf(stderr, "Failed to read from input: %s\n", err)
			return err
		}

		err = n.normalizeRow(record)
		if err != nil {
			fmt.Fprintf(stderr, "Invalid record at line %d: %s\n", lineNumber, err)
			continue
		}

		writer.Write(record)
	}
}

func (n *Normalizer) normalizeRow(row []string) error {
	// {"Timestamp", "Address", "ZIP", "FullName", "FooDuration", "BarDuration", "TotalDuration", "Notes"},
	var (
		timestampData     = strings.ToValidUTF8(row[0], string(unicode.ReplacementChar))
		addressData       = strings.ToValidUTF8(row[1], string(unicode.ReplacementChar))
		zipData           = strings.ToValidUTF8(row[2], string(unicode.ReplacementChar))
		fullNameData      = strings.ToValidUTF8(row[3], string(unicode.ReplacementChar))
		fooDurationData   = strings.ToValidUTF8(row[4], string(unicode.ReplacementChar))
		barDurationData   = strings.ToValidUTF8(row[5], string(unicode.ReplacementChar))
		totalDurationData = strings.ToValidUTF8(row[6], string(unicode.ReplacementChar))
		notesData         = strings.ToValidUTF8(row[7], string(unicode.ReplacementChar))
	)

	{
		timestamp, err := time.Parse("1/2/06 3:04:05 PM", timestampData)
		if err != nil {
			return fmt.Errorf("invalid Timestamp: %w", err)
		}
		timestamp = time.Date(
			timestamp.Year(),
			timestamp.Month(),
			timestamp.Day(),
			timestamp.Hour(),
			timestamp.Minute(),
			timestamp.Second(),
			timestamp.Nanosecond(),
			n.inputTimezone,
		)
		timestampData = timestamp.In(n.outputTimezone).Format(time.RFC3339)
	}

	{
		zip, err := strconv.ParseInt(zipData, 10, 32)
		if err != nil {
			return fmt.Errorf("invalid ZipCode: %w", err)
		}
		zipData = fmt.Sprintf("%0.5d", zip)
	}

	{
		fullNameData = strings.ToUpper(fullNameData)
	}

	var fooDuration time.Duration
	{
		var hours, minutes, seconds, milliseconds time.Duration
		_, err := fmt.Sscanf(fooDurationData, "%d:%d:%d.%d", &hours, &minutes, &seconds, &milliseconds)
		if err != nil {
			return fmt.Errorf("invalid FooDuration %q: %w", fooDurationData, err)
		}

		fooDuration = time.Hour*hours + time.Minute*minutes + time.Second*seconds + time.Millisecond*milliseconds
		fooDurationData = strconv.FormatFloat(fooDuration.Seconds(), 'f', -1, 64)
	}

	var barDuration time.Duration
	{
		var hours, minutes, seconds, milliseconds time.Duration
		_, err := fmt.Sscanf(barDurationData, "%d:%d:%d.%d", &hours, &minutes, &seconds, &milliseconds)
		if err != nil {
			return fmt.Errorf("invalid BarDuration %q: %w", barDurationData, err)
		}

		barDuration = time.Hour*hours + time.Minute*minutes + time.Second*seconds + time.Millisecond*milliseconds
		barDurationData = strconv.FormatFloat(barDuration.Seconds(), 'f', -1, 64)
	}

	{
		totalDurationData = strconv.FormatFloat((barDuration + fooDuration).Seconds(), 'f', -1, 64)
	}

	row[0] = timestampData
	row[1] = addressData
	row[2] = zipData
	row[3] = fullNameData
	row[4] = fooDurationData
	row[5] = barDurationData
	row[6] = totalDurationData
	row[7] = notesData

	return nil
}

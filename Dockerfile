FROM golang as build

COPY . /workdir

RUN cd /workdir && go build -o normalizer

ENTRYPOINT /workdir/normalizer
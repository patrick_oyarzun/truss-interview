## Running the project locally

1. Install go from https://golang.org
2. Build the program with `go build -o normalizer`
3. Run the binary as desired `./normalizer < sample.csv > output.csv`

## Running tests

1. Install go from https://golang.org
2. Run the tests with `go test ./... -v`

```
$ go test ./... -v
?   	truss-interview	[no test files]
=== RUN   TestNormalizer
Running Suite: Normalizer Suite
===============================
Random Seed: 1631648760
Will run 14 of 14 specs

••••••••••••••
Ran 14 of 14 Specs in 0.002 seconds
SUCCESS! -- 14 Passed | 0 Failed | 0 Pending | 0 Skipped
--- PASS: TestNormalizer (0.00s)
PASS
ok  	truss-interview/lib/normalizer	0.009s

```

## Running the project with docker

In case you have any difficulty with the above, I've included a Dockerfile to build and run the project.

1. Build the docker image with `docker build -t normalizer .`

```
$ docker build -t normalizer .
Sending build context to Docker daemon  2.376MB
Step 1/4 : FROM golang as build
 ---> ec365f06285d
Step 2/4 : COPY . /workdir
 ---> 4d1288151174
Step 3/4 : RUN cd /workdir && go build -o normalizer
 ---> Running in c28659a90235
Removing intermediate container c28659a90235
 ---> 682bc8471f69
Step 4/4 : ENTRYPOINT /workdir/normalizer
 ---> Running in 180537d77f4c
Removing intermediate container 180537d77f4c
 ---> cfc777f7149a
Successfully built cfc777f7149a
Successfully tagged normalizer:latest
```

2. Run the image like so `docker run -i --rm normalizer < sample.csv  > output.csv`

```
$ docker run -i --rm normalizer < sample.csv 
Timestamp,Address,ZIP,FullName,FooDuration,BarDuration,TotalDuration,Notes
2011-04-01T14:00:00-04:00,"123 4th St, Anywhere, AA",94121,MONKEY ALBERTO,5012.123,5553.123,10565.246,I am the very model of a modern major general
2014-03-12T03:00:00-04:00,"Somewhere Else, In Another Time, BB",00001,SUPERMAN ÜBERTAN,401012.123,5553.123,406565.246,This is some Unicode right here. ü ¡! 😀
2016-02-29T15:11:11-05:00,111 Ste. #123123123,01101,RÉSUMÉ RON,113012.123,5553.123,118565.246,🏳️🏴🏳️🏴
2011-01-01T03:00:01-05:00,"This Is Not An Address, BusyTown, BT",94121,MARY 1,5012.123,0,5012.123,I like Emoji! 🍏🍎😍
2017-01-01T02:59:59-05:00,"123 Gangnam Style Lives Here, Gangnam Town",31403,ANTICIPATION OF UNICODE FAILURE,5012.123,5553.123,10565.246,I like Math Symbols! ≱≰⨌⊚
2011-11-11T14:11:11-05:00,überTown,10001,PROMPT NEGOTIATOR,5012.123,5553.123,10565.246,"I’m just gonna say, this is AMAZING. WHAT NEGOTIATIONS."
2010-05-12T19:48:12-04:00,Høøük¡,01231,SLEEPER SERVICE,5012.123,5553.123,10565.246,2/1/22
2012-10-06T01:31:11-04:00,"Test Pattern Town, Test Pattern, TP",00121,株式会社スタジオジブリ,5012.123,5553.123,10565.246,1:11:11.123
2016-03-13T03:01:00-04:00,The Moon,00011,HERE WE GO,5012.123,5553.123,10565.246,
```

package main

import (
	"log"
	"os"
	"truss-interview/lib/normalizer"
)

func main() {
	norm, err := normalizer.NewNormalizer()
	if err != nil {
		log.Println("Failed to initialize:", err)
	}

	err = norm.Normalize(os.Stdin, os.Stdout, os.Stderr)
	if err != nil {
		log.Println("Failed to normalize:", err)
	}
}
